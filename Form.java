package reto.Choucair.Tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import reto.Choucair.userinterface.FormPage;

public class Form implements Task {
    public static Form OnThePage() { return Tasks.instrumented(Form.class); }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(FormPage.LOGIN_BUTTON),
                Enter.theValue("IngresarNombre").into(FormPage.INPUT_NAME),
                Enter.theValue("IngresaApellido*").into(FormPage.INPUT_LAST),
                Enter.theValue("IngresarEmail").into(FormPage.INPUT_EMAIL),
                Enter.theValue("IngresaCumpleaños").into(FormPage.INPUT_BIRTH),
                Enter.theValue("IngresarCiudad").into(FormPage.INPUT_CITY),
                Enter.theValue("IngresarPostal").into(FormPage.INPUT_POSTAL),
                Enter.theValue("IngresarPais").into(FormPage.INPUT_COUNTRY),
                Enter.theValue("IngresaComputador").into(FormPage.INPUT_COMPU),
                Enter.theValue("IngresarVersion").into(FormPage.INPUT_VERSION),
                Enter.theValue("IngresarLenguaje").into(FormPage.INPUT_LANG),
                Enter.theValue("IngresarCelular").into(FormPage.INPUT_MOBILE),
                Enter.theValue("IngresarModelo").into(FormPage.INPUT_MODEL),
                Enter.theValue("IngresarSistema").into(FormPage.INPUT_OPERATION),
                Enter.theValue("IngresaContraseña").into(FormPage.INPUT_PASSWORD),
                Enter.theValue("IngresaTuContraseña*").into(FormPage.INPUT_PASSWORD*),
                Click.on(FormPage.ENTER_BUTTON)
        );
    }
}
